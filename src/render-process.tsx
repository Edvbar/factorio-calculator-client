import React from "react";
import DOM from "react-dom";
import App from "core/index";
import "assets/css/style.less";

DOM.render(<App />, document.getElementById("root"));
