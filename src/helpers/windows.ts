import { remote } from "electron";

const { BrowserWindow } = remote;

export const focusedWindow = () => BrowserWindow.getFocusedWindow();
export const close = (): void => focusedWindow()?.close();
export const maximize = () => focusedWindow()?.maximize();
export const minimize = () => focusedWindow()?.minimize();
export const sizing = () =>
  focusedWindow()?.isMaximized ? minimize() : maximize();
