const path = require("path");

function pathing(location) {
  return path.join(__dirname, location);
}

function pathingAll(locations) {
  let rez = [];
  Object.keys(locations).map((key) => {
    rez[key] = pathing(locations[key]);
  });
  return { ...rez };
}

module.exports = { pathing, pathingAll };
