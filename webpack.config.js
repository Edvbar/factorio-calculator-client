const { modes } = require("./config/server.json");
const { pathingAll, pathing } = require("./helpers/pathing");
const { alias, files } = require("./config/filePath.json");
const createMainConfig = require("./config/webpack/mainConfig");
const createRenderConfig = require("./config/webpack/renderConfig");

module.exports = function (env) {
  const isDev = !!env.development;
  const target = env.target;
  const configFactory =
    target == "main" ? createMainConfig : createRenderConfig;

  console.log(
    "\n##\n## BUILDING BUNDLE FOR: " +
      (target == "main" ? "main process" : "render process") +
      "\n## CONFIGURATION: " +
      (isDev ? modes.dev : modes.prod) +
      "\n##\n"
  );

  return {
    context: pathing(files.src),
    mode: isDev ? modes.dev : modes.prod,
    output: {
      filename: "[name].js",
      path: pathing(files.dist),
    },
    resolve: {
      extensions: [".js", ".jsx", ".ts", ".tsx", ".json", ".css", ".scss"],
      alias: pathingAll(alias),
    },
    ...configFactory(isDev),
  };
};
