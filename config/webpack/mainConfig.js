const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const DefinePlugin = require("webpack").DefinePlugin;
const CopyWebpackPlugin = require("copy-webpack-plugin");
const { modes } = require("../server.json");

module.exports = function createMainConfig(isDev) {
  return {
    target: "electron-main",
    entry: {
      "main-process": "./main-process.ts",
    },
    module: {
      rules: [
        {
          test: /\.(ts|js)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-typescript"],
            },
          },
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: ["main-process.*.js"],
      }),
      new DefinePlugin({
        ENVIRONMENT: JSON.stringify(isDev ? modes.dev : modes.prod),
      }),
      new CopyWebpackPlugin({
        patterns: [{ from: "package.json", to: "./", context: "../" }],
      }),
    ],
  };
};
