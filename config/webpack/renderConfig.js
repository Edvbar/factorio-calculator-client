const HtmlPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const { server, devServer } = require("../server.json");
const { files, htmlFilePath } = require("../filePath.json");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = function createRenderConfig(isDev) {
  return {
    target: "electron-renderer",
    devtool: isDev ? "source-map" : undefined,
    entry: {
      "render-process": "./render-process.tsx",
    },
    module: {
      rules: [
        {
          test: /\.less$/,
          use: [MiniCssExtractPlugin.loader, "css-loader", "less-loader"],
        },
        {
          test: /\.(jpg|jpeg|png|gif|svg)$/,
          use: [{ loader: "url-loader?limit=100000" }],
        },
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-typescript", "@babel/preset-react"],
              plugins: [
                "@babel/plugin-proposal-class-properties",
                "@babel/plugin-transform-runtime",
                isDev && require.resolve("react-refresh/babel"),
              ].filter(Boolean),
            },
          },
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: ["**/*", "!main-process.*.js"],
      }),
      new HtmlPlugin({
        filename: `${htmlFilePath.entry}.${htmlFilePath.extention}`,
        template: `${htmlFilePath.entry}.${htmlFilePath.extention}`,
        cache: true,
      }),
      new MiniCssExtractPlugin(),
      isDev && new webpack.HotModuleReplacementPlugin(),
      isDev && new ReactRefreshWebpackPlugin(),
    ].filter(Boolean),
    devServer: isDev
      ? {
          contentBase: files.dist,
          compress: devServer.compress,
          hot: devServer.hot,
          port: server.port,
        }
      : undefined,
  };
};
